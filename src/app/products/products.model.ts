
export interface partidos{
    juego: string;
    estadio: string;
    fecha: string;
    marcador: string;
    goles: number;
    anotadores: string;
    tarjetas: string;
    lesionados: string;
    codigo: number;
    proveedor: proveedor[];
}
export interface proveedor{
    nombre: string;
    cedula: number;
    direccion: string;
    numero: number;
    correo:string;
    codigo: number;
}

