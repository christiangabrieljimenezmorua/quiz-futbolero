import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  formProdutAdd: FormGroup;
  constructor(
    private serviceProduct: ProductService,
    private router: Router
    ) { }

  ngOnInit() {
    this.formProdutAdd = new FormGroup({
      pjuego: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators: [Validators.required, Validators.minLength(3)]
        }
      ),
      pestadio: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators:[Validators.required, Validators.minLength(3)]
        }
      ),
      pfecha: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators:[Validators.required, Validators.minLength(3)]
        }
      ),
      pmarcador: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators:[Validators.required, Validators.minLength(3)]
        }
      ),
      pgoles: new FormControl(
        1,
        {
          updateOn: 'blur',
          validators:[Validators.required, Validators.min(1)]
        }
      ),
      panotadores: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators:[Validators.required]
        }
      ),
      ptarjetas: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators:[Validators.required]
        }
      ),
      plesionados: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators:[Validators.required]
        }
      ),
      pcodigo: new FormControl(
        1,
        {
          updateOn: 'blur',
          validators:[Validators.required, Validators.min(1)]
        }
      )
    });
  }
  addProduct(){
    if(!this.formProdutAdd.valid){
      return;
    }
    this.serviceProduct.addProduct(
      this.formProdutAdd.value.pjuego,
      this.formProdutAdd.value.pestadio, 
      this.formProdutAdd.value.pfecha,
      this.formProdutAdd.value.pmarcador,
      this.formProdutAdd.value.pgoles,
      this.formProdutAdd.value.panotadores,
      this.formProdutAdd.value.ptarjetas,
      this.formProdutAdd.value.plesionados,
      this.formProdutAdd.value.pcodigo

    );
    this.formProdutAdd.reset();
    this.router.navigate(['/products']);
  }

}
