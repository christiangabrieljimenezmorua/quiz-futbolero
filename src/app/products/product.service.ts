import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { partidos, proveedor } from './products.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private proveedore: proveedor[] = [
    {
        nombre: "Intcomex",
        cedula: 11111,
        direccion: "San Jose",
        numero: 8888,
        correo: "www@deee.com",
        codigo: 333
    },
    {
        nombre: "Intcomex",
        cedula: 11111,
        direccion: "San Jose",
        numero: 8888,
        correo: "www@deee.com",
        codigo: 333
    }
  ];
  private partidos:partidos [] = [
    {
      proveedor: this.proveedore,
      juego:"Santos vs Jicaral",
      estadio: "Ebal Rodriguez",
      fecha: "18-12-2020",
      marcador: "1-0",
      goles: 1,
      anotadores: "Osvaldo R",
      tarjetas: "2 amarillas / 1 roja",
      lesionados: "No hubieron lesionados",
      codigo: 1
    },
    {
      proveedor: this.proveedore,
      juego:"Cartago vs Grecia",
      estadio: "Fello Meza",
      fecha: "19-12-2020",
      marcador: "2-1",
      goles: 3,
      anotadores: "Allen G, Byron B y Alejando R",
      tarjetas: "2 amarillas / 1 roja",
      lesionados: "Roger C",
      codigo: 2
    }
  ];
  constructor(
    private http: HttpClient
  ) { }
  getAll(){
    return [...this.partidos];
  }
  getProduct(productId: number){
    return {
      ...this.partidos.find(
        product => {
          return product.codigo === productId;
        }
      )
    };
  }
  deleteProduct(productId: number){
    this.partidos = this.partidos.filter(
      product => {
        return product.codigo !== productId;
      }
    );
  }
  addProduct(pjuego: string,
    pestadio: string,
    pfecha: string,
    pmarcador: string,
    pgoles: number,
    panotadores: string,
    ptarjetas: string,
    plesionados: string,
    pcodigo: number){

      const partido: partidos = {
        proveedor: this.proveedore,
        juego: pjuego,
        estadio: pestadio,
        fecha: pfecha,
        marcador: pmarcador,
        goles: pgoles,
        anotadores: panotadores,
        tarjetas: ptarjetas,
        lesionados: plesionados,
        codigo: pcodigo
      }
      this.http.post('https://clasesmiercoles-49dde.firebaseio.com/addProduct.json',
        { 
          ...partido, 
          id:null
        }).subscribe(() => {
          console.log('entro');
        });
      this.partidos.push(partido);
      
  }
}
